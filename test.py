import requests
import json
import _thread
from pprint import pprint


def make_a_request():
    result = requests.post(
        "http://localhost:8080/api/experimental/dags/example_python_operator/dag_runs",
        data=json.dumps("{}"),
        auth=("airflow", "airflow"))
    pprint(result.content.decode('utf-8'))


_thread.start_new_thread(make_a_request, ())
